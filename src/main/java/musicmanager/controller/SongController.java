package musicmanager.controller;

import musicmanager.model.Song;
import musicmanager.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/song")
public class SongController {

    @Autowired
    private SongService songService;

    @GetMapping("/all")
    @ResponseBody
    public Iterable<Song> all() { return songService.all();}
    //public SongController(SongService songService){this.songService = songService;}

    @GetMapping("/title/{songTitle}")
    @ResponseBody
    public Optional<Song> findByTitle(@PathVariable(name = "songTitle") String songTitle){return songService.findByTitle(songTitle);}

    @GetMapping("/id/{songId}")
    @ResponseBody
    public Optional<Song> findById(@PathVariable(name = "songId") Long songId){return songService.findById(songId);}

    @GetMapping("/like/{songLike}")
    @ResponseBody
    public Iterable<Song> findByTitleLike(@PathVariable(name = "songLike") String songLike){return songService.findByTitleLike(songLike);}

    @GetMapping("/artist/{artistName}")
    @ResponseBody
    public Iterable<Song> findByArtistName(@PathVariable(name = "artistName") String artistName){return songService.findByArtistName(artistName);}

    @PostMapping("/create")
    public Song create(Song song) {return songService.create(song);}

    @PostMapping("/delete")
    public void delete(Song song) {songService.delete(song);}
}

