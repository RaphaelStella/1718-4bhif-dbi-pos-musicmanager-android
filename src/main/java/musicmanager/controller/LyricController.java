package musicmanager.controller;

import musicmanager.model.Lyric;
import musicmanager.service.LyricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/lyric")
public class LyricController {

    @Autowired
    private LyricService lyricService;

    @GetMapping("/all")
    @ResponseBody
    public Iterable<Lyric> all(){return lyricService.all();}

    @GetMapping("/title/{lyricTitle}")
    @ResponseBody
    public Optional<Lyric> findByName(@PathVariable(name = "lyricTitle") String lyricTitle){return lyricService.findByName(lyricTitle);}

    @GetMapping("/id/{lyricId}")
    @ResponseBody
    public Optional<Lyric> findById(@PathVariable(name = "lyricId") Long lyricId){return lyricService.findById(lyricId);}

    @GetMapping("/like/{lyricLike}")
    @ResponseBody
    public Iterable<Lyric> findByNameLike(@PathVariable(name = "lyricLike") String lyricLike){return lyricService.findByNameLike(lyricLike);}

    @PostMapping("/create")
    public Lyric create(@RequestBody Lyric lyric) {return lyricService.create(lyric);}
}
