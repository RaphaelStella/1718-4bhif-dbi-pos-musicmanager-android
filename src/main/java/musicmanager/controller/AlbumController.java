package musicmanager.controller;

import musicmanager.model.Album;
import musicmanager.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/album")
public class AlbumController {

    @Autowired
    private AlbumService albumService;

    @GetMapping("/all")
    @ResponseBody
    public Iterable<Album> all() {return albumService.all();}

    @GetMapping("/title/{albumTitle}")
    @ResponseBody
    public Optional<Album> findByTitle(@PathVariable(name = "albumTitle") String albumTitle){return albumService.findByTitle(albumTitle);}

    @GetMapping("/id/{albumId}")
    @ResponseBody
    public Optional<Album> findById (@PathVariable(name = "albumId") Long albumId) {return albumService.findById(albumId);}

    @GetMapping("/artist/{artistName}")
    @ResponseBody
    public Iterable<Album> findByArtist (@PathVariable(name = "artistName") String artistName){return albumService.findByArtist(artistName);}

    @GetMapping("/release/{releaseYear}")
    @ResponseBody
    public Iterable<Album> findByReleaseYear (@PathVariable(name = "releaseYear") int releaseYear){return albumService.findByReleaseYear(releaseYear);}

    @PostMapping("/create")
    public Album create(@RequestBody Album album) {
        return albumService.create(album);
    }

    @PostMapping("/deleteByTitle/{title}")
    public void deleteByTitle(@PathVariable(name = "title") String title) {
        albumService.deleteByTitle(title);
    }


}
