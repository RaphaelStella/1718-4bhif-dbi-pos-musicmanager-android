package musicmanager.controller;

import musicmanager.model.Artist;
import musicmanager.service.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/artist")
public class ArtistController {

    @Autowired
    private ArtistService artistService;

    @GetMapping("/all")
    @ResponseBody
    public Iterable<Artist> all() {return artistService.all();}

    @GetMapping("/name/{artistName}")
    @ResponseBody
    public Optional<Artist> findByName(@PathVariable(name = "artistName") String artistName){return artistService.findByName(artistName);}

    @GetMapping("/id/{artistId}")
    @ResponseBody
    public Optional<Artist> findById (@PathVariable(name = "artistId") Long artistId) {return artistService.findById(artistId);}

    @GetMapping("/like/{artistLike}")
    @ResponseBody
    public Iterable<Artist> findByNameLike (@PathVariable(name = "artistLike") String artistLike){return artistService.findByNameLike(artistLike);}

    @PostMapping("/create")
    public Artist create(@RequestBody Artist artist) {
        return artistService.create(artist);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody Artist artist) {artistService.delete(artist); }

}
