package musicmanager.controller;

import musicmanager.model.Playlist;
import musicmanager.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/playlist")
public class PlaylistController {

    @Autowired
    public PlaylistService playlistService;

    @GetMapping("/all")
    @ResponseBody
    public Iterable<Playlist> all() {return playlistService.all();}

    @GetMapping("/{id}")
    @ResponseBody
    public Optional<Playlist> findById(@PathVariable(name = "id") Long id){return playlistService.findById(id);}
}

