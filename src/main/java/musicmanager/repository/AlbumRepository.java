package musicmanager.repository;

import musicmanager.model.Album;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AlbumRepository extends Neo4jRepository<Album, Long> {

    Optional<Album> findByTitle(String title);

    Optional<Album> findById(Long id);

    Iterable<Album> findByReleaseyear(int year);

    Iterable<Album> findByArtist(String artistname);

    void deleteByTitle(String title);
}