package musicmanager.repository;

import musicmanager.model.Playlist;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlaylistRepository extends Neo4jRepository<Playlist, Long> {

    Optional<Playlist> findByName(String name);

    Optional<Playlist> findById(Long id);

    Iterable<Playlist> findByNameLike(String name);

}