package musicmanager.repository;

import musicmanager.model.Artist;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ArtistRepository extends Neo4jRepository<Artist, Long> {

    Optional<Artist> findByName(String name);

    Optional<Artist> findById(Long id);

    Iterable<Artist> findByNameLike(String name);

}
