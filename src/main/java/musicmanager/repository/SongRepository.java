package musicmanager.repository;

import musicmanager.model.Song;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SongRepository extends Neo4jRepository<Song, Long> {

    Optional<Song> findByTitle(String title);

    Optional<Song> findById(Long id);

    Iterable<Song> findByTitleLike(String title);

    Iterable<Song> findByArtistName(String artistName);
}
