package musicmanager.repository;

import musicmanager.model.Lyric;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LyricRepository extends Neo4jRepository<Lyric, Long> {

    Optional<Lyric> findByName(String name);

    Optional<Lyric> findById(Long id);

    Iterable<Lyric> findByNameLike(String name);

}

