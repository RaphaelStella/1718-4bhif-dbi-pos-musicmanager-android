package musicmanager.model;

import lombok.*;
import org.neo4j.ogm.annotation.*;
import org.springframework.lang.Nullable;

import java.util.HashMap;

@NodeEntity
@AllArgsConstructor
@Builder
@ToString
@RequiredArgsConstructor
public class Playlist {

    @Id
    @GeneratedValue
    @Getter
    private Long id;

    @Property(name = "name")
    @Nullable
    @Getter
    @Setter
    private String name;

    @Relationship(type = "CONTAINS")
    @Nullable
    @Getter
    @Setter
    private HashMap songs;
}