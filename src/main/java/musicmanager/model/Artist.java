package musicmanager.model;

import lombok.*;
import org.neo4j.ogm.annotation.*;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@NodeEntity
@AllArgsConstructor
@Builder
@ToString
@RequiredArgsConstructor
public class Artist {

    @Id
    @GeneratedValue
    @Getter
    public Long id;

    @Property(name = "name")
    @Nullable
    @Getter
    @Setter
    private String name;

    @Property (name = "urlToImage")
    @Nullable
    @Getter
    @Setter
    private String urlToImage;

    @Property (name = "urlToSpotify")
    @Nullable
    @Getter
    @Setter
    private String urlToSpotify;

    @Relationship(type = "OWNS")
    @Nullable
    @Getter
    @Setter
    private List<Song> songs;

    @Relationship(type = "OWNS")
    @Nullable
    @Getter
    @Setter
    private List<Album> albums;

}