package musicmanager.model;

import lombok.*;
import org.neo4j.ogm.annotation.*;
import org.springframework.lang.Nullable;

@NodeEntity
@AllArgsConstructor
@Builder
@ToString
@RequiredArgsConstructor
public class Lyric {

    @Id
    @GeneratedValue
    @Getter
    private Long id;

    @Property(name = "name")
    @Nullable
    @Getter
    @Setter
    private String name;

    @Property (name = "lyric")
    @Nullable
    @Getter
    @Setter
    private String lyric;

    @Relationship(type = "IS_LYRIC_OF")
    @Nullable
    @Getter
    @Setter
    private Song song;
}

