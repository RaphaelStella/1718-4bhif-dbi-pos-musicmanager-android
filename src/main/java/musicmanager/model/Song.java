package musicmanager.model;

import lombok.*;
import org.neo4j.ogm.annotation.*;
import org.springframework.lang.Nullable;

@NodeEntity
@AllArgsConstructor
@Builder
@ToString
@RequiredArgsConstructor
public class Song {

    @Id
    @GeneratedValue
    @Getter
    private Long id;

    @Property(name = "title")
    @Nullable
    @Getter
    @Setter
    private String title;

    @Relationship(type = "OWNDBY", direction = Relationship.INCOMING)
    @Nullable
    @Getter
    @Setter
    private Artist artist;

    @Relationship(type = "BELONGS_TO")
    @Nullable
    @Getter
    @Setter
    private Album album;

    @Property (name = "duration")
    @Nullable
    @Getter
    @Setter
    private String duration;

    @Property (name = "pathtofile")
    @Nullable
    @Getter
    @Setter
    //@Convert(PathConverter.class) wenn noch zeit bleibt https://www.youtube.com/watch?v=gqCqurf79bM
    private String pathtofile;
}