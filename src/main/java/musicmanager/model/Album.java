package musicmanager.model;

import lombok.*;
import org.neo4j.ogm.annotation.*;
import org.springframework.lang.Nullable;

import java.util.List;

@NodeEntity
@AllArgsConstructor
@Builder
@ToString
@RequiredArgsConstructor
public class Album {

    @Id
    @GeneratedValue
    @Getter
    private Long id;

    @Property(name = "title")
    @Nullable
    @Getter
    @Setter
    private String title;

    @Property (name = "releaseyear")
    @Nullable
    @Getter
    @Setter
    private int releaseyear;

    @Property (name = "cover")
    @Nullable
    @Getter
    @Setter
    private byte[] cover;

    @Relationship(type = "OWNS",direction = Relationship.INCOMING)
    @Nullable
    @Getter
    @Setter
    private List<Song> songs;

    @Relationship(type = "BELONGS",direction = Relationship.INCOMING)
    @Nullable
    @Getter
    @Setter
    private Artist artist;
}

