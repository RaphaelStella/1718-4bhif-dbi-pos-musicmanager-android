package musicmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
/*
	docker container run --name neo4j -d --publish 7474:7474 --publish 7687:7687 neo4j:3.4
*/
