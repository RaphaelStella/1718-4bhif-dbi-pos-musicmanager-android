package musicmanager.service;

import musicmanager.model.Artist;
import musicmanager.repository.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ArtistService {

    @Autowired
    private ArtistRepository artistRepository;

    public Iterable<Artist> all() {return artistRepository.findAll();}

    public Optional<Artist> findByName(String name){ return artistRepository.findByName(name); }

    public Optional<Artist> findById(Long id){
        return artistRepository.findById(id);
    }

    public Iterable<Artist> findByNameLike(String name) {
        return artistRepository.findByNameLike(name);
    }

    public Artist create(Artist artist) { return artistRepository.save(artist);}

    public void delete(Artist artist) { artistRepository.delete(artist);}
}
