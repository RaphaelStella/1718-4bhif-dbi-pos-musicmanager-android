package musicmanager.service;

import musicmanager.model.Playlist;
import musicmanager.repository.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PlaylistService {

    @Autowired
    private PlaylistRepository playlistRepository;

    public Iterable<Playlist> all() {return playlistRepository.findAll();}

    public Optional<Playlist> findByName(String name){
        return playlistRepository.findByName(name);
    }

    public Optional<Playlist> findById(Long id){
        return playlistRepository.findById(id);
    }

    public Iterable<Playlist> findByNameLike(String name) {
        return playlistRepository.findByNameLike(name);
    }
}

