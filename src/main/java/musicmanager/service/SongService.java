package musicmanager.service;

import musicmanager.model.Song;
import musicmanager.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SongService {

    @Autowired
    private SongRepository songRepository;

    public Iterable<Song> all() {
        return songRepository.findAll();
    }


    public Optional<Song> findByTitle(String title) {
        return songRepository.findByTitle(title);
    }

    public Optional<Song> findById(Long id) {
        return songRepository.findById(id);
    }

    public Iterable<Song> findByTitleLike(String title) {
        return songRepository.findByTitleLike(title);
    }

    public Iterable<Song> findByArtistName(String nameOfArtist) {
        return songRepository.findByArtistName(nameOfArtist);
    }

    public Song create(Song song) { return songRepository.save(song);}

    public void delete(Song song) {songRepository.delete(song);}


}