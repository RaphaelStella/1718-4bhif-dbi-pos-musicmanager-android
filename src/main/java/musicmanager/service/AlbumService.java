package musicmanager.service;

import musicmanager.model.Album;
import musicmanager.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AlbumService {

    @Autowired
    private AlbumRepository albumRepository;

    public Iterable<Album> all() {return albumRepository.findAll();}

    public Optional<Album> findByTitle(String title){
        return albumRepository.findByTitle(title);
    }

    public Optional<Album> findById(Long id){
        return albumRepository.findById(id);
    }

    public Iterable<Album> findByArtist (String artistName){
        return albumRepository.findByArtist(artistName);
    }

    public Iterable<Album> findByReleaseYear(int year){
        return albumRepository.findByReleaseyear(year);
    }

    public Album create(Album album) { return albumRepository.save(album);}

    public void delete(Album album) { albumRepository.delete(album);}

    public void deleteByTitle(String title) { albumRepository.deleteByTitle(title);}
}
