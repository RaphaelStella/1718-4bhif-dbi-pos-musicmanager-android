package musicmanager.service;

import musicmanager.model.Lyric;
import musicmanager.repository.LyricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LyricService {

    @Autowired
    private LyricRepository lyricRepository;

    public Iterable<Lyric> all() {return lyricRepository.findAll();}

    public Optional<Lyric> findByName(String name){
        return lyricRepository.findByName(name);
    }

    public Optional<Lyric> findById(Long id){
        return lyricRepository.findById(id);
    }

    public Iterable<Lyric> findByNameLike(String name) {
        return lyricRepository.findByNameLike(name);
    }

    public Lyric create(Lyric lyric) { return lyricRepository.save(lyric);}
}