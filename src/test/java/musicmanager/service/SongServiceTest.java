package musicmanager.service;

import musicmanager.model.Album;
import musicmanager.model.Artist;
import musicmanager.model.Song;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.ArrayList;


@SpringBootTest
@RunWith(SpringRunner.class)
public class SongServiceTest {

    @Autowired
    SongService songService;

    @Test
    public void findByTitle() {

        Song song = Song.builder().title("Testtitle").build();

        songService.create(song);

        Assert.notNull(songService.findByTitle(song.getTitle()));
    }

    @Test
    public void create() {
        Song song = Song.builder().title("TestName").build();

        Song song1 = songService.create(song);

        org.junit.Assert.assertEquals(song,song1);
    }

    @Test
    public void delete()
    {
        Song song = Song.builder().title("Test").build();

        songService.create(song);
        songService.delete(song);

        org.junit.Assert.assertFalse(songService.findByTitle(song.getTitle()).isPresent());
    }
}