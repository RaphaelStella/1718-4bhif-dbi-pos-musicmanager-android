package musicmanager.service;

import musicmanager.model.Album;
import musicmanager.model.Artist;
import musicmanager.model.Song;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;


import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ArtistServiceTest {

    @Autowired
    private ArtistService artistService;

    /*@Test
    public void findByName()
    {
        Artist artist = Artist.builder().name("Mustermann").build();

        artistService.create(artist);

        Assert.notNull(artistService.findByName(artist.getName()));
    }*/

    @Test
    public void create() {
        ArrayList<Song> songs = new ArrayList<Song>();
        ArrayList<Album> albums = new ArrayList<Album>();

        Artist artist = Artist.builder().name("TestName").songs(songs).albums(albums).build();

        Artist artist1 = artistService.create(artist);

        org.junit.Assert.assertEquals(artist,artist1);
    }

    @Test
    public void delete()
    {
        Artist artist = Artist.builder().name("Test").build();

        artistService.create(artist);
        artistService.delete(artist);

        org.junit.Assert.assertFalse(artistService.findByName(artist.getName()).isPresent());
    }
}