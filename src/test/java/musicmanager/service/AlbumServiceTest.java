package musicmanager.service;

import musicmanager.model.Album;
import musicmanager.model.Song;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.ArrayList;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AlbumServiceTest {

    @Autowired
    private AlbumService albumService;

    @Test
    public void findByTitle()
    {
        ArrayList<Song> songs = new ArrayList<Song>();

        Album album = Album.builder().title("TestAlbum").songs(songs).releaseyear(2017).build();

        albumService.create(album);

        Assert.notNull(albumService.findByTitle(album.getTitle()));
    }

    @Test
    public void delete()
    {
        Album album = Album.builder().title("Test").build();

        albumService.create(album);
        albumService.delete(album);

        org.junit.Assert.assertFalse(albumService.findByTitle(album.getTitle()).isPresent());
    }

}