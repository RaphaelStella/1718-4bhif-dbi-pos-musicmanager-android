package musicmanager;

import musicmanager.model.Album;
import musicmanager.model.Artist;
import musicmanager.model.Song;
import musicmanager.repository.AlbumRepository;
import musicmanager.repository.ArtistRepository;
import musicmanager.repository.SongRepository;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ApplicationTest {

    ArtistRepository artistRepository;
    AlbumRepository albumRepository;
    SongRepository songRepository;

    @Test
    public void main() {
        inserter();
        Assert.assertTrue(true);
    }

    private void inserter() {
        /* Test Daten in resourceordner*/
        Long id = null;
        int counter = 1;

        Artist artist;
        Album album;
        Song song = null;


        try {
            java.io.BufferedReader FileReader=
                    new java.io.BufferedReader(
                            new java.io.FileReader(
                                    new java.io.File("C:\\Users\\rapha\\Desktop\\music.txt")
                            )
                    );

            String zeile="";

            while(null!=(zeile=FileReader.readLine())){
                String[] split=zeile.split(";");

                artist = Artist.builder().name(split[1]).build();
                album = Album.builder().title(split[2]).releaseyear(Integer.parseInt(split[4])).artist(artist).build();
                song = Song.builder().title(split[0]).artist(artist).album(album).duration(split[3]).build();

                artistRepository.save(artist);
                albumRepository.save(album);
                songRepository.save(song);

                if(counter == 5)
                {
                   id =  song.getId();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}