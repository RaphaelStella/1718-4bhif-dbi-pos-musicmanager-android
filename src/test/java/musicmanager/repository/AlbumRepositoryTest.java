package musicmanager.repository;

import musicmanager.model.Album;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AlbumRepositoryTest {

    @Autowired
    public AlbumRepository albumRepository;

    @Test
    public void createAlbumRepo()
    {
        Album album = Album.builder().title("Test_Album").build();
        albumRepository.save(album);

        Assert.assertNotNull(albumRepository.findByTitle("Test_Album"));
    }

}