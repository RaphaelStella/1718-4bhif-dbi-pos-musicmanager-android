package musicmanager.repository;

import musicmanager.model.Artist;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ArtistRepositoryTest {

    @Autowired
    public ArtistRepository artistRepository;

    @Test
    public void createArtistRepoTest()
    {
        Artist artist = Artist.builder().name("Mustermann").build();
        artistRepository.save(artist);

        Assert.assertNotNull(artistRepository.findById(artist.getId()));
    }
}