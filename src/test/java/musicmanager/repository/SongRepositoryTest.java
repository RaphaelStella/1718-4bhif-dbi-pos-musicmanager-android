package musicmanager.repository;

import musicmanager.model.Album;
import musicmanager.model.Artist;
import musicmanager.model.Song;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.text.html.Option;
import java.util.Optional;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SongRepositoryTest {

    @Autowired
    public SongRepository songRepository;
    
    /*@Autowired
    public AlbumRepository albumRepository;
    
    @Autowired
    public ArtistRepository artistRepository;*/

    @Test
    public void createSongRepoTest()
    {
        Song song = Song.builder().title("Default_Test").build();
        songRepository.save(song);

        Assert.assertNotNull(songRepository.findById(song.getId()));
    }

}