package musicmanager.model;

import org.junit.Assert;
import org.junit.Test;

public class SongTest {

    @Test
    public void createSong()
    {
        Song song = Song.builder().title("All Mine").artist(
                Artist.builder().name("Kanye West").urlToImage("https://www.billboard.com/files/styles/article_main_image/public/media/kanye-west-2015-a-billboard-1548.jpg").urlToSpotify("https://open.spotify.com/artist/5K4W6rqBFWDnAN6FQUkS6x?si=JanEMZBeSxWCJYva0nGGGA").build()).duration("2:26").pathtofile("https://open.spotify.com/track/3qnoOm4fwZPBS116f5hpgF?si=7Qvh4rYoTX-0fT_1YTOMHg")
                .build();
        Assert.assertNotNull(song);
    }
}