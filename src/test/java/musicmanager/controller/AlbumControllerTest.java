package musicmanager.controller;

import musicmanager.model.Album;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;


import static com.sun.tools.doclint.Entity.and;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.nio.charset.Charset;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@WebAppConfiguration
@SpringBootTest
@RunWith(SpringRunner.class)
public class AlbumControllerTest {

    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Before
    public void setup() {this.mockMvc = webAppContextSetup(webApplicationContext).build();}

    public static final MediaType APPLICATION_JSON_UTF8_OWN = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );

    @Test
    public void all() throws Exception {

        mockMvc.perform(get("/album/all")
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk()) ;
    }

    @Test
    public void findByTitle() throws Exception {
        mockMvc.perform(get("/album/title/Double Or Nothing")
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk()).andExpect(jsonPath("$.title", Matchers.is("Double Or Nothing"))) ;
    }

    @Test
    public void findById() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/album/id/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk()).andExpect(jsonPath("$.title", Matchers.is("I'm The Man (Remix)")));
    }

    /*@Test
    public void findByArtist() {
    }*/

    /*@Test
    public void findByReleaseYear() {
    }*/

    @Test
    public void create() throws Exception {
        Album album = Album.builder().title("Default_Test").build();

        MvcResult mvcResult = mockMvc.perform(post("/album/create")
                .contentType(APPLICATION_JSON_UTF8_OWN))
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();
    }
}