package musicmanager.controller;

import musicmanager.model.Song;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@WebAppConfiguration
@SpringBootTest
@RunWith(SpringRunner.class)
public class SongControllerTest {

    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Before
    public void setup()
    {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    public static final MediaType APPLICATION_JSON_UTF8_OWN = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );

    @Test
    public void all() throws Exception {
        mockMvc.perform(get("/song/all")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }


    @Test
    public void create() throws Exception {
        String jsonString = "{" + "\"tile\":\"Default_Test\"" + "}";

        Song song = Song.builder().title("Default_Test").build();


        MvcResult mvcResult = mockMvc.perform(post("/song/create")
                .contentType(APPLICATION_JSON_UTF8_OWN))
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();
    }

    @Test
    public void findByName() throws Exception {
        mockMvc.perform(get("/song/title/I'm The Man (Remix)")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",Matchers.is(2)));
    }
}